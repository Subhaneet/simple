import { Routes } from '@angular/router';
import { ComicComponent } from './app/comic/comic.component';
import { ImageDetailComponent } from './app/image-detail/image-detail.component';
import { LoginComponent } from './app/login/login.component';
import { UploadComponent } from './app/upload/upload.component';
import { AuthenticationGuard } from './app/services/authenticationGuard.service';
import { SignupFormComponent } from './app/signup-form/signup-form.component';

export const appRoutes : Routes = [
  {path: 'comic', component: ComicComponent, canActivate: [AuthenticationGuard]},
  {path: 'upload', component: UploadComponent, canActivate: [AuthenticationGuard]},
  {path: 'image/:id', component: ImageDetailComponent, canActivate: [AuthenticationGuard]},
  {path: '', redirectTo: '/comic' , pathMatch: 'full'},
  {path: 'signup', component: SignupFormComponent},
  {path: 'login', component: LoginComponent}
];
