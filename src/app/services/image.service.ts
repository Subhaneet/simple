import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import 'firebase/storage';
import {ComicImage } from '../models/comicImage.model';
import * as firebase from 'firebase/app';
import { Action } from 'rxjs/internal/scheduler/Action';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private uid: string;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.afAuth.authState.subscribe(auth => {
      if(auth !== undefined && auth !== null) {
        this.uid = auth.uid;
      }
    });
  }

  getImages() : Observable<ComicImage[]>{
    return this.db.list('uploads').snapshotChanges().map( actions => {
      return actions.map( action => ({$key: action.key,...action.payload.val()}));
    });
  }

  getImage(key: string){
    return firebase.database().ref('uploads/' + key).once('value')
      .then((snap) => snap.val());
  }

}
