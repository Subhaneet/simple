import { Injectable } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import {ComicImage } from '../models/comicImage.model';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Upload } from '../models/upload.model';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { unusedValueExportToPlacateAjd } from '@angular/core/src/render3/interfaces/i18n';

@Injectable()

export class UploadService {

  private basePath: string = '/uploads';
  private uploads : AngularFireList<ComicImage[]>;
  private uploadTask: firebase.storage.UploadTask;
  images: Observable<ComicImage[]>;
  constructor(private ngFire: AngularFireModule, private db: AngularFireDatabase) { }

  uploadFile(upload: Upload) {
    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);

    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      }, (error) => {
        console.log("Error")
      }, () => {
        upload.url = this.uploadTask.snapshot.downloadURL;
        upload.name = upload.file.name;
        this.saveFileData(upload);

      });
    }
  deleteUpload(upload: Upload){
    this.deleteFileData(upload.$key)
    .then(() => {
      this.deleteFileStorage(upload.name)
    }).catch(error => {
      console.log(error)
    });
  }
  public saveFileData(upload: Upload){
    this.db.list(`${this.basePath}/`).push(upload);
    console.log("File saved!: "+upload.url);
  }

  public deleteFileData(key: string){
    return this.db.list(`${this.basePath}/`).remove(key);
  }

  private deleteFileStorage(name: string){
    let storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete()
  }
}
