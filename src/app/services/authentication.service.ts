import { Injectable } from '@angular/core';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app'
import { User } from '../models/user.model';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private user: Observable<firebase.User>;
  private authState: any;

  constructor(private afAuth: AngularFireAuth, private db : AngularFireDatabase , private router : Router) {
    this.user = afAuth.authState;
  }

  get currentUserId(): string {
    return this.authState != null ? this.authState.uid : '';
  }

  signUp (email : string, password : string , displayName : string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
        const status = 'Reading mode';
        this.setUserData(email, displayName, status);
      }).catch (error => console.log(error));
  }

  setUserData (email: string, displayName : string , status : string): void {
    const path = 'users/${this.currentUserId}';
    const data = {
      email : email,
      displayName : displayName,
      status : status
    };

    this.db.object(path).update(data)
      .catch(error => console.log(error));
  }

  setUserStatus (status : string): void {
    const path = 'users/${this.currentUserId}';
    const data = {
      status : status
    };
  }

  login(user: User){
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then((resolve) => {
        const status = "Reading";
        this.setUserStatus(status);
        this.router.navigate(['']);
      });
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  authUser() {
    return this.user;
  }
}
