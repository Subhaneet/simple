import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {

  email: string;
  password : string;
  displayName : string;
  errorMsg : string;

  constructor(private authenticationService: AuthenticationService, private router : Router) { }

  signUp() {
    const email = this.email;
    const password = this.password;
    const displayName = this.displayName;
    this.authenticationService.signUp(email, password, displayName)
      .then(resolve => this.router.navigate(['comic']))
      .catch(error => this.errorMsg = error.message);
  }
}
