export interface ComicImage {
  $key?: string;
  name?: string;
  url?: string;
}
