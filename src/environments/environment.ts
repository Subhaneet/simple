// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyBjIj7vACbuh-E0JEyvHa7M8s_WJezVaIU',
    authDomain: 'comic-art-sharing-system.firebaseapp.com',
    databaseURL: 'https://comic-art-sharing-system.firebaseio.com',
    projectId: 'comic-art-sharing-system',
    storageBucket: 'comic-art-sharing-system.appspot.com',
    messagingSenderId: '162075496148',
    appId: '1:162075496148:web:dd27606aac390901'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
